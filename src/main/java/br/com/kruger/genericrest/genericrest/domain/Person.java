package br.com.kruger.genericrest.genericrest.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "person")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) // json config
public class Person extends BaseModel {

	private static final long serialVersionUID = 1L;

	private String name;
	private String nationalDocument;
	private String email;
	private String phone;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNationalDocument() {
		return nationalDocument;
	}

	public void setNationalDocument(String nationalDocument) {
		this.nationalDocument = nationalDocument;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}
