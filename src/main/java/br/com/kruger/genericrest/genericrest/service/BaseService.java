package br.com.kruger.genericrest.genericrest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.kruger.genericrest.genericrest.dao.BaseDAO;
import br.com.kruger.genericrest.genericrest.domain.BaseModel;

public abstract class BaseService<T extends BaseModel> {

	@Autowired
	private BaseDAO<T> dao;

	public List<T> findAll() {
		return dao.findAll();
	}	

	public T saveOrUpdate(T entity) {
		return dao.save(entity);
	}
	
	public void deleteById(long id) {
		dao.deleteById(id);
	}
	
	public T getOne(long id) {
		return dao.getOne(id);
	}

}
