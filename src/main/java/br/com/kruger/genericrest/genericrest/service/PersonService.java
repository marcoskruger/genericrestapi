package br.com.kruger.genericrest.genericrest.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.kruger.genericrest.genericrest.domain.Person;

@Service
@Transactional(rollbackFor = Throwable.class)
public class PersonService extends BaseService<Person>{

}
