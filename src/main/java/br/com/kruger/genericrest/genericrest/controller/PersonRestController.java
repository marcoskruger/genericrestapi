package br.com.kruger.genericrest.genericrest.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.kruger.genericrest.genericrest.domain.Person;

@RestController
@RequestMapping("/persons")
public class PersonRestController extends BaseRestController<Person> {

}
