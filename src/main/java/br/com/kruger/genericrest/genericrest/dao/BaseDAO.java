package br.com.kruger.genericrest.genericrest.dao;

import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;

import br.com.kruger.genericrest.genericrest.domain.BaseModel;

public interface BaseDAO<T extends BaseModel> extends JpaRepository<T, Serializable> {

}
