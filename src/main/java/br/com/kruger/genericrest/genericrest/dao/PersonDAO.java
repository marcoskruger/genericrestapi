package br.com.kruger.genericrest.genericrest.dao;

import br.com.kruger.genericrest.genericrest.domain.Person;

public interface PersonDAO extends BaseDAO<Person> {

}
