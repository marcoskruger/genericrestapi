package br.com.kruger.genericrest.genericrest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.kruger.genericrest.genericrest.domain.BaseModel;
import br.com.kruger.genericrest.genericrest.service.BaseService;

public abstract class BaseRestController<T extends BaseModel> {

	@Autowired
	private BaseService<T> service;

	@RequestMapping(method = RequestMethod.GET)
	public final List<T> list() {
		return service.findAll();
	}

	@RequestMapping(method = RequestMethod.POST)
	public final T create(@RequestBody T entity) {
		return service.saveOrUpdate(entity);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	public final T update(@PathVariable(value = "id") long id, @RequestBody T entity) {
		return service.saveOrUpdate(entity);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	public final void delete(@PathVariable(value = "id") long id) {
		service.deleteById(id);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public final T get(@PathVariable(value = "id") long id) {
		return service.getOne(id);
	}
}
